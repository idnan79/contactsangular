import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ContactsPage } from './contacts/contacts.page';
import { ContactCreatePage } from './contact-create/contact-create.page';

//conatctsList our contac
//create contact -create a new contact

const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    redirectTo: '/contacts',
  },
  {
    path: 'contacts',
    component: ContactsPage,
  },
  {
    path: 'contacts/create',
    component: ContactCreatePage,
  },
];
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
